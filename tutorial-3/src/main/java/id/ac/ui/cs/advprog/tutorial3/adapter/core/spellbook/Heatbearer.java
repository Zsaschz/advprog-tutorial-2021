package id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook;


public class Heatbearer implements Spellbook {

    private String holderName;
    private Boolean isCharged;

    public Heatbearer(String holderName) {
        this.isCharged = false;
        this.holderName = holderName;
    }

    @Override
    public String smallSpell() {
        isCharged = false;
        return "Enemy scarred";
    }

    @Override
    public String largeSpell() {
        if(!isCharged){
            isCharged = true;
            return "EXPUULOOOOSHHHIOONNNN!";
        } else {
            return "Only one explosion a day!";
        }
    }

    @Override
    public String getName() {
        return "Heat Bearer";
    }

    @Override
    public String getHolderName() { return holderName; }
}