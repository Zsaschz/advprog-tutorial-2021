package id.ac.ui.cs.advprog.tutorial3.adapter.core.bow;

public class IonicBow implements Bow {

    private String holderName;
    private Boolean isAimMode;

    public IonicBow(String holderName) {
        this.holderName = holderName;
        this.isAimMode = false;
    }

    @Override
    public String setIsAimMode() {
        this.isAimMode = !this.isAimMode;
        if (isAimMode) {
            return "Entering aim shot mode";
        } else {
            return "Hip shot mode enabled";
        }
    }

    @Override
    public Boolean getIsAimMode() {
        return this.isAimMode;
    }

    @Override
    public String shootArrow(boolean isAimShot) {
        if(isAimShot) {
            return "Arrow reacted with the enemy's protons";
        } else {
            return "Separated one atom from the enemy";
        }
    }

    @Override
    public String getName() {
        return "Ionic Bow";
    }

    @Override
    public String getHolderName() { return holderName; }
}