package id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook;

public class TheWindjedi implements Spellbook {

    private String holderName;
    private Boolean isCharged;

    public TheWindjedi(String holderName) {
        this.isCharged = false;
        this.holderName = holderName;
    }

    @Override
    public String smallSpell() {
        isCharged = false;
        return "Small musical attack launched";
    }

    @Override
    public String largeSpell() {
        if (!isCharged) {
            isCharged = true;
            return "Orchestra-class music attack launched";
        } else {
            return "The conductor is still preparing the orchestra!";
        }
    }

    @Override
    public String getName() {
        return "The Windjedi";
    }

    @Override
    public String getHolderName() { return holderName; }
}