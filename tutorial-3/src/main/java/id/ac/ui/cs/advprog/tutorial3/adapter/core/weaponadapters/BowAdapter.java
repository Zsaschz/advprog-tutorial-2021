package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;

// DONE: complete me :)
public class BowAdapter implements Weapon {
    private Bow bow;

    public BowAdapter(Bow bow) {
        this.bow = bow;
    }

    @Override
    public String normalAttack() {
        return bow.shootArrow(bow.getIsAimMode());
    }

    @Override
    public String chargedAttack() {
        return bow.setIsAimMode();
    }

    @Override
    public String getName() {
        return bow.getName();
    }

    @Override
    public String getHolderName() {
        // DONE: complete me
        return bow.getHolderName();
    }
}
