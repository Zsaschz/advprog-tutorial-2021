package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;

/**
 * Kelas ini melakukan ROT13 cipher
 */
public class ROT13Transformation {
    private int offset;

    public ROT13Transformation() {
        this.offset = 13;
    }

    public Spell encode(Spell spell) {
        return process(spell, true);
    }

    public Spell decode(Spell spell) {
        return process(spell, false);
    }

    private Spell process(Spell spell, boolean encode) {
        String text = spell.getText();
        Codex codex = spell.getCodex();
        int selector = encode ? 1 : -1;
        StringBuilder res = new StringBuilder();
        for(int i = 0; i < text.length(); i++) {
            char oldChar = text.charAt(i);
            int charIdx = codex.getIndex(oldChar);
            int newCharIdx = charIdx + (selector * offset);
            newCharIdx = newCharIdx < 0 ? newCharIdx + codex.getCharSize() : newCharIdx % codex.getCharSize();
            res.append(codex.getChar(newCharIdx));
        }

        return new Spell(res.toString(), codex);
    }
}
