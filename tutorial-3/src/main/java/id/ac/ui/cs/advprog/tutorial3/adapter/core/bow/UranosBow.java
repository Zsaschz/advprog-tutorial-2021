package id.ac.ui.cs.advprog.tutorial3.adapter.core.bow;

public class UranosBow implements Bow {

    private String holderName;
    private Boolean isAimMode;

    public UranosBow(String holderName) {
        this.holderName = holderName;
        this.isAimMode = false;
    }

    @Override
    public String setIsAimMode() {
        this.isAimMode = !this.isAimMode;
        if (isAimMode) {
            return "Entering aim shot mode";
        } else {
            return "Hip shot mode enabled";
        }
    }

    @Override
    public Boolean getIsAimMode() {
        return this.isAimMode;
    }

    @Override
    public String shootArrow(boolean isAimShot) {
        if(isAimShot) {
            return "Gaining charge... gaining speed... headshot!";
        } else {
            return "headshot!";
        }
    }

    @Override
    public String getName() {
        return "Uranos Bow";
    }

    @Override
    public String getHolderName() { return holderName; }

}