package id.ac.ui.cs.advprog.tutorial3.adapter.core.bow;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

// DONE: add tests
public class UranosBowTest {
    private Class<?> uranosBowClass;

    @BeforeEach
    public void setUp() throws Exception {
        uranosBowClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.UranosBow");
    }

    @Test
    public void testUranosBowIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(uranosBowClass.getModifiers()));
    }

    @Test
    public void testUranosBowIsABow() {
        Collection<Type> interfaces = Arrays.asList(uranosBowClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow")));
    }

    @Test
    public void testUranosBowOverrideShootArrowMethod() throws Exception {
        Class<?>[] shootArrowArgs = new Class[1];
        shootArrowArgs[0] = boolean.class;
        Method shootArrow = uranosBowClass.getDeclaredMethod("shootArrow", shootArrowArgs);

        assertEquals("java.lang.String",
                shootArrow.getGenericReturnType().getTypeName());
        assertEquals(1,
                shootArrow.getParameterCount());
        assertTrue(Modifier.isPublic(shootArrow.getModifiers()));
    }

    @Test
    public void testUranosBowOverrideGetNameMethod() throws Exception {
        Method getName = uranosBowClass.getDeclaredMethod("getName");

        assertEquals("java.lang.String",
                getName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getName.getParameterCount());
        assertTrue(Modifier.isPublic(getName.getModifiers()));
    }

    @Test
    public void testUranosBowOverrideGetHolderMethod() throws Exception {
        Method getHolderName = uranosBowClass.getDeclaredMethod("getHolderName");

        assertEquals("java.lang.String",
                getHolderName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getHolderName.getParameterCount());
        assertTrue(Modifier.isPublic(getHolderName.getModifiers()));
    }

    // DONE: buat test untuk menguji hasil dari pemanggilan method
    @Test
    public void testIonicBowOverrideGetIsAimModeMethod() throws Exception {
        Method getIsAimMode = uranosBowClass.getDeclaredMethod("getIsAimMode");

        assertEquals("java.lang.Boolean",
                getIsAimMode.getGenericReturnType().getTypeName());
        assertEquals(0,
                getIsAimMode.getParameterCount());
        assertTrue(Modifier.isPublic(getIsAimMode.getModifiers()));
    }

    @Test
    public void testIonicBowOverrideSetIsAimModeMethod() throws Exception {
        Method setIsAimMode = uranosBowClass.getDeclaredMethod("setIsAimMode");

        assertEquals("java.lang.String",
                setIsAimMode.getGenericReturnType().getTypeName());
        assertEquals(0,
                setIsAimMode.getParameterCount());
        assertTrue(Modifier.isPublic(setIsAimMode.getModifiers()));
    }

    @Test
    public void testIonicBowGetIsAim() throws Exception {
        Method getIsAimMode = uranosBowClass.getDeclaredMethod("getIsAimMode");
        Boolean isAimMode = new UranosBow("dummy").getIsAimMode();

        assertEquals("java.lang.Boolean", getIsAimMode.getGenericReturnType().getTypeName());
        assertEquals(false, isAimMode);

    }

    @Test
    public void testIonicBowSetIsAim() throws Exception {
        UranosBow dummy = new UranosBow("dummy");
        String ret = dummy.setIsAimMode();
        Boolean isAimMode = dummy.getIsAimMode();

        assertEquals(true, isAimMode);
        assertEquals("Entering aim shot mode", ret);

        String ret2 = dummy.setIsAimMode();
        Boolean isAimMode2 = dummy.getIsAimMode();

        assertEquals(false, isAimMode2);
        assertEquals("Hip shot mode enabled", ret2);


    }

    @Test
    public void testIonicBowGetName() throws Exception {
        UranosBow dummy = new UranosBow("dummy");
        String name = dummy.getName();

        assertEquals("Uranos Bow", name);
    }

    @Test
    public void testIonicBowGetHolderName() throws Exception {
        IonicBow dummy = new IonicBow("dummy");
        String holderName = dummy.getHolderName();

        assertEquals("dummy", holderName);
    }

    @Test
    public void testIonicBowShootArrowIsAimShotTrue() throws Exception {
        UranosBow dummy = new UranosBow("dummy");
        String shootArrow = dummy.shootArrow(true);

        assertEquals("Gaining charge... gaining speed... headshot!", shootArrow);
    }

    @Test
    public void testIonicBowShootArrowIsAimShotFalse() throws Exception {
        UranosBow dummy = new UranosBow("dummy");
        String shootArrow = dummy.shootArrow(false);

        assertEquals("headshot!", shootArrow);
    }
}
