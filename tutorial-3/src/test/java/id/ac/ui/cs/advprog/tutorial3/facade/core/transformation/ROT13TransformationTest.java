package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ROT13TransformationTest {
    private Class<?> rot13Class;

    @BeforeEach
    public void setup() throws Exception {
        rot13Class = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.ROT13Transformation");
    }

    @Test
    public void testROT13HasEncodeMethod() throws Exception {
        Method translate = rot13Class.getDeclaredMethod("encode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testROT13EncodesCorrectly() throws Exception {
        String text = "Safira and I went to a blacksmith to forge our sword";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "fnsv4nMn0qMVM9r06M61MnMoynpx5zv6uM61Ms14trM174M5914q";

        Spell result = new ROT13Transformation().encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testAbyssalHasDecodeMethod() throws Exception {
        Method translate = rot13Class.getDeclaredMethod("decode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testAbyssalDecodesCorrectly() throws Exception {
        String text = "fnsv4nMn0qMVM9r06M61MnMoynpx5zv6uM61Ms14trM174M5914q";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Safira and I went to a blacksmith to forge our sword";

        Spell result = new ROT13Transformation().decode(spell);
        assertEquals(expected, result.getText());
    }
}
