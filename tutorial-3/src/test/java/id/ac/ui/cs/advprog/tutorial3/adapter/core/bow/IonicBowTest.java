package id.ac.ui.cs.advprog.tutorial3.adapter.core.bow;

import com.sun.org.apache.xpath.internal.operations.Bool;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

// DONE: add tests
public class IonicBowTest {
    private Class<?> ionicBowClass;

    @BeforeEach
    public void setUp() throws Exception {
        ionicBowClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.IonicBow");
    }

    @Test
    public void testIonicBowIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(ionicBowClass.getModifiers()));
    }

    @Test
    public void testIonicBowIsABow() {
        Collection<Type> interfaces = Arrays.asList(ionicBowClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow")));
    }

    @Test
    public void testIonicBowOverrideShootArrowMethod() throws Exception {
        Class<?>[] shootArrowArgs = new Class[1];
        shootArrowArgs[0] = boolean.class;
        Method shootArrow = ionicBowClass.getDeclaredMethod("shootArrow", shootArrowArgs);

        assertEquals("java.lang.String",
                shootArrow.getGenericReturnType().getTypeName());
        assertEquals(1,
                shootArrow.getParameterCount());
        assertTrue(Modifier.isPublic(shootArrow.getModifiers()));
    }

    @Test
    public void testIonicBowOverrideGetNameMethod() throws Exception {
        Method getName = ionicBowClass.getDeclaredMethod("getName");

        assertEquals("java.lang.String",
                getName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getName.getParameterCount());
        assertTrue(Modifier.isPublic(getName.getModifiers()));
    }

    @Test
    public void testIonicBowOverrideGetHolderMethod() throws Exception {
        Method getHolderName = ionicBowClass.getDeclaredMethod("getHolderName");

        assertEquals("java.lang.String",
                getHolderName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getHolderName.getParameterCount());
        assertTrue(Modifier.isPublic(getHolderName.getModifiers()));
    }

    @Test
    public void testIonicBowOverrideGetIsAimModeMethod() throws Exception {
        Method getIsAimMode = ionicBowClass.getDeclaredMethod("getIsAimMode");

        assertEquals("java.lang.Boolean",
                getIsAimMode.getGenericReturnType().getTypeName());
        assertEquals(0,
                getIsAimMode.getParameterCount());
        assertTrue(Modifier.isPublic(getIsAimMode.getModifiers()));
    }

    @Test
    public void testIonicBowOverrideSetIsAimModeMethod() throws Exception {
        Method setIsAimMode = ionicBowClass.getDeclaredMethod("setIsAimMode");

        assertEquals("java.lang.String",
                setIsAimMode.getGenericReturnType().getTypeName());
        assertEquals(0,
                setIsAimMode.getParameterCount());
        assertTrue(Modifier.isPublic(setIsAimMode.getModifiers()));
    }

    // DONE: buat test untuk menguji hasil dari pemanggilan method
    @Test
    public void testIonicBowGetIsAim() throws Exception {
        Method getIsAimMode = ionicBowClass.getDeclaredMethod("getIsAimMode");
        Boolean isAimMode = new IonicBow("dummy").getIsAimMode();

        assertEquals("java.lang.Boolean", getIsAimMode.getGenericReturnType().getTypeName());
        assertEquals(false, isAimMode);

    }

    @Test
    public void testIonicBowSetIsAim() throws Exception {
        IonicBow dummy = new IonicBow("dummy");
        String ret = dummy.setIsAimMode();
        Boolean isAimMode = dummy.getIsAimMode();

        assertEquals(true, isAimMode);
        assertEquals("Entering aim shot mode", ret);

        String ret2 = dummy.setIsAimMode();
        Boolean isAimMode2 = dummy.getIsAimMode();

        assertEquals(false, isAimMode2);
        assertEquals("Hip shot mode enabled", ret2);


    }

    @Test
    public void testIonicBowGetName() throws Exception {
        IonicBow dummy = new IonicBow("dummy");
        String name = dummy.getName();

        assertEquals("Ionic Bow", name);
    }

    @Test
    public void testIonicBowGetHolderName() throws Exception {
        IonicBow dummy = new IonicBow("dummy");
        String holderName = dummy.getHolderName();

        assertEquals("dummy", holderName);
    }

    @Test
    public void testIonicBowShootArrowIsAimShotTrue() throws Exception {
        IonicBow dummy = new IonicBow("dummy");
        String shootArrow = dummy.shootArrow(true);

        assertEquals("Arrow reacted with the enemy's protons", shootArrow);
    }

    @Test
    public void testIonicBowShootArrowIsAimShotFalse() throws Exception {
        IonicBow dummy = new IonicBow("dummy");
        String shootArrow = dummy.shootArrow(false);

        assertEquals("Separated one atom from the enemy", shootArrow);
    }
}
