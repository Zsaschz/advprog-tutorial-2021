package id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

// DONE: add tests
public class TheWindjediTest {
    private Class<?> theWindjediClass;

    @BeforeEach
    public void setUp() throws Exception {
        theWindjediClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.TheWindjedi");
    }

    @Test
    public void testTheWindjediIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(theWindjediClass.getModifiers()));
    }

    @Test
    public void testTheWindjediIsASpellbook() {
        Collection<Type> interfaces = Arrays.asList(theWindjediClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook")));
    }

    @Test
    public void testTheWindjediOverrideSmallSpellMethod() throws Exception {
        Method smallSpell = theWindjediClass.getDeclaredMethod("smallSpell");

        assertEquals("java.lang.String",
                smallSpell.getGenericReturnType().getTypeName());
        assertEquals(0,
                smallSpell.getParameterCount());
        assertTrue(Modifier.isPublic(smallSpell.getModifiers()));
    }

    @Test
    public void testTheWindjediOverrideLargeSpellMethod() throws Exception {
        Method largeSpell = theWindjediClass.getDeclaredMethod("largeSpell");

        assertEquals("java.lang.String",
                largeSpell.getGenericReturnType().getTypeName());
        assertEquals(0,
                largeSpell.getParameterCount());
        assertTrue(Modifier.isPublic(largeSpell.getModifiers()));
    }

    @Test
    public void testTheWindjediOverrideGetNameMethod() throws Exception {
        Method getName = theWindjediClass.getDeclaredMethod("getName");

        assertEquals("java.lang.String",
                getName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getName.getParameterCount());
        assertTrue(Modifier.isPublic(getName.getModifiers()));
    }

    @Test
    public void testTheWindjediOverrideGetHolderMethod() throws Exception {
        Method getHolderName = theWindjediClass.getDeclaredMethod("getHolderName");

        assertEquals("java.lang.String",
                getHolderName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getHolderName.getParameterCount());
        assertTrue(Modifier.isPublic(getHolderName.getModifiers()));
    }

    // DONE: buat test untuk menguji hasil dari pemanggilan method
    @Test
    public void testTheWindjediGetName() throws Exception {
        TheWindjedi dummy = new TheWindjedi("dummy");
        String name = dummy.getName();

        assertEquals("The Windjedi", name);
    }

    @Test
    public void testTheWindjediGetHolderName() throws Exception {
        TheWindjedi dummy = new TheWindjedi("dummy");
        String holderName = dummy.getHolderName();

        assertEquals("dummy", holderName);
    }

    @Test
    public void testTheWindjediSmallSpell() throws Exception {
        TheWindjedi dummy = new TheWindjedi("dummy");
        String smallSpell = dummy.smallSpell();

        assertEquals("Small musical attack launched", smallSpell);
    }

    @Test
    public void testTheWindjediLargeSpellIsChargedFalse() throws Exception {
        TheWindjedi dummy = new TheWindjedi("dummy");
        String largeSpell = dummy.largeSpell();

        assertEquals("Orchestra-class music attack launched", largeSpell);
    }

    @Test
    public void testTheWindjediLargeSpellDouble() throws Exception {
        TheWindjedi dummy = new TheWindjedi("dummy");
        dummy.largeSpell();
        String largeSpell = dummy.largeSpell();

        assertEquals("The conductor is still preparing the orchestra!", largeSpell);
    }
}
